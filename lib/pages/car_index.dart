import 'package:flutter/material.dart';
import '../model/car.dart';
import '../model/tip.dart';

class CarIndex extends StatelessWidget {
  CarIndex({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          TopTip(
            tip: "立即认证",
            content: "请提前汽车实名认证，需人工审核",
          ),
          Column(
            children: <Widget>[
              CarCard(
                title: "雪铁龙-C3-XL",
                subTitle: '银座·5座·新能源',
                content: '续航230公里',
                type: 1,
                licensePlate: "京A00001",
              ),
              CarCard(
                title: "马自达-昂克赛拉",
                subTitle: '高配·4座·新能源',
                content: '续航180公里',
                type: 2,
                licensePlate: "京A00002",
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.only(left: 12.0,right: 12.0),
            margin: EdgeInsets.only(top: 10.0),
            child: FlatButton(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(25.0, 15.0, 25.0, 15.0),
              shape: RoundedRectangleBorder(
                side: BorderSide.none,
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
              onPressed: (){ print("查看用车规则"); },
              child: Text("用车规则 >",style: TextStyle(
                  color: Color.fromRGBO(253, 155, 28, 1)
              ),),
            ),
          )
        ],
      ),
    );
  }
}