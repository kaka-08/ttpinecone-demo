import 'package:flutter/material.dart';


class BikeIndex extends StatelessWidget {

  final String title;

  const BikeIndex({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(this.title),
    );
  }
}
