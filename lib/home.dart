import 'package:flutter/material.dart';
import 'pages/car_index.dart';
import 'pages/bike_index.dart';
//import 'package:permission_handler/permission_handler.dart';

class HomePage extends StatefulWidget {

  final String title;

  const HomePage({Key key, this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

const TextStyle textStyle = TextStyle(color: Color.fromRGBO(51, 51, 51, 1));


const List<String> tabs = <String>['电单车','汽车'];

class _HomePageState extends State<HomePage> {

  void _getLocation() async {
//    PermissionStatus permission = await PermissionHandler().checkPermissionStatus(PermissionGroup.location);
//    bool hasPermission = permission == PermissionStatus.granted;
//    if(!hasPermission){
//    Map<PermissionGroup, PermissionStatus> map = await PermissionHandler().requestPermissions([
//    PermissionGroup.location
//    ]);
//    if(map.values.toList()[0] != PermissionStatus.granted){
//    return;
//    }
//    }
//    BaiduLocation location = await FlutterBaiduMap.getCurrentLocation();
  }

  @override
  void initState() {
    super.initState();
    _getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          leading: IconButton(
            icon: Icon(Icons.account_circle),
            tooltip: "首页",
            onPressed: () {},
          ),
          actions: <Widget>[IconButton(
            icon: Icon(Icons.search),
            tooltip: '搜索',
            onPressed: (){  },
          ),],
          bottom: TabBar(
            isScrollable: true,
            labelColor: Color.fromRGBO(249, 199, 0, 1),
            unselectedLabelColor: Colors.white,
            unselectedLabelStyle: textStyle,
            tabs: tabs.map((String title) {
              return Tab(
                text: title,
              );
            }).toList(),
          ),
        ),
        body: TabBarView(
//          children: tabs.map((Choice choice) {
//            return ChoiceCard(choice: choice);
//          }).toList(),
          children: <Widget>[
            BikeIndex(title: "这是电单车的首页，假装有地图吧!"),
            CarIndex()
          ],
        ),
//        floatingActionButton: FloatingActionButton(
//          onPressed: _getLocation,
//          tooltip: 'Increment',
//          child: Icon(Icons.add),
//        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}

