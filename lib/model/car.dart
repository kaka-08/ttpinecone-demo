import 'package:flutter/material.dart';

class CarCard extends StatelessWidget {

  final String title;
  final String subTitle;
  final String content;
  final int type;
  final String licensePlate;

  CarCard({Key key, this.title,this.subTitle,this.content,this.type, this.licensePlate}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          side: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(10.0))
      ),
      margin: EdgeInsets.symmetric(vertical: 10.0,horizontal: 10.0),
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 15.0,horizontal: 25.0),
                height: 145,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(this.title,style: TextStyle(
                            fontSize: 22,fontWeight: FontWeight.bold
                        ),),
                        SizedBox(height: 8.0,),
                        Text(this.subTitle,style: TextStyle(
                          fontSize: 15,color: Color.fromRGBO(51, 51, 51, 1),
                        ),),
                        SizedBox(height: 5.0,),
                        Text(this.content,style: TextStyle(
                          fontSize: 15,color: Color.fromRGBO(51, 51, 51, 1),
                        ),),
                        SizedBox(height: 5.0,),
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 1.0,horizontal: 3.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Color.fromRGBO(227, 227, 227, 1), width: 1),
                            color: Color.fromRGBO(245, 247, 248, 1),
                          ),
                          child: Text(this.licensePlate,style: TextStyle(
                              color: Color.fromRGBO(152, 152, 152, 1),
                              fontSize: 12.0
                          ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                  right: 15.0,
                  top: 15.0,
                  child: Container(
                    child: Text(this.type == 1 ? "经济型": '舒适型'),
                    padding: EdgeInsets.symmetric(vertical: 3.0,horizontal: 5.0),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Color.fromRGBO(102, 102, 102, 1))
                    ),
                  )
              ),
              Positioned(
                right: 25.0,
                top: 15.0,
                child: Container(
                  width: 160,
                  height: 80,
                  margin: EdgeInsets.only(top: 35),
                  child: Image.asset(
                    "images/car01.png",
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
          Divider(height: 1.0,),
          Container(
            padding: EdgeInsets.only(top: 4,bottom: 6),
            child: Row(
              children: <Widget>[
                FlatButton(
                  padding: EdgeInsets.only(left: 25.0),
                  onPressed: (){ print("查看计费规则"); },
                  child: Text("查看计费规则",style: TextStyle(
                      color: Color.fromRGBO(51, 51, 51, 1)
                  ),),
                ),
                SizedBox(width: 91.0),
                Container(
                  width: 175,
                  child: RaisedButton(
                    color: Color.fromRGBO(249, 199, 0, 1),
                    onPressed: (){ print("选择此车"); },
                    shape: RoundedRectangleBorder(
                        side: BorderSide.none,
                        borderRadius: BorderRadius.all(Radius.circular(50))
                    ),
                    child: Text("选择此车"),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}


