import 'package:flutter/material.dart';

class TopTip extends StatelessWidget {

  final String content;
  final String tip;

  const TopTip({Key key, this.content, this.tip}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
            height: 44.0,
            color: Color.fromRGBO(255, 234, 205, 1),
            child: Padding(
              padding: EdgeInsets.only(left: 20.0),
              child: Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.all(12.0),
                      child: Text(
                          this.content,
                          style: TextStyle(
                            color: Color.fromRGBO(254, 68, 3, 1),
                          )
                      ),
                    ),
                    Positioned(
                      height: 30.0,
                      right: 20.0,
                      top: 7.0,
                      child: Container(
                        color: Color.fromRGBO(254, 68, 3, 1),
                        child: FlatButton(
                          child: Text(this.tip,style: TextStyle(
                              color: Colors.white
                          ),),
                        ),
                      ),
                    )
                  ]
              ),
            ),
          ),
        ),
      ],
    );
  }
}
